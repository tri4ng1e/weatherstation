#include "../inc/libSPI.h"

void spi_init(){
	SPI_PORT&=~(1<<SCK|1<<MOSI);
	SPI_DDR|=1<<SCK|1<<MOSI; SPI_DDR&=~(1<<MISO);
}

// send 1 byte and receive 1 byte
uint8_t spi_fast_shift(uint8_t send){
	uint8_t recv=0;
	for(signed char i=7; i>=0; i--){
		if (bitGet(send, i)==1) OUT_hi;
		else OUT_lo;
		SCK_hi;
		if (IN_get==1) recv|=1<<i;
		SCK_lo;
	}
	return recv;
}

// send array of bytes
void spi_transmit_sync(uint8_t * dataout, uint8_t len){
	for (uint8_t i = 0; i < len; i++) {
		/*for(signed char j=7; j>=0; j--){
			if (bitGet(dataout[i], i)==1) OUT_hi;
			else OUT_lo;
			SCK_pulse;
		}*/
		spi_fast_shift(dataout[i]);
	}
}
