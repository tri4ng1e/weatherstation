
/*		nRF24L01+ Library, file nrf.h
	Author: Sky-WaLkeR (aka shadowalker)
	  Date: 11.06.2013 21:56
	Tested: ATmega8 @ 8Mhz

	Description:
  description goes here

	Notes:
  Each function in nrf.cpp has comments, read it!
  
*/
#ifndef _H_NRF_INC_
#define _H_NRF_INC_

#include "libSPI.h"
#include "nrf_defs.h"
#include <avr/io.h>
#include <util/delay.h>

/*  SETTINGS  */
#define NRF_PORT PORTB
#define NRF_DDR	 DDRB
#define NRF_CE 	 0
#define NRF_CSN  1

#define nrf_def_payload 16
#define nrf_def_ch		2
#define nrf_def_config	1<<EN_CRC|1<<MASK_MAX_RT|1<<MASK_TX_DS



extern uint8_t _nrf_mode;
#define NRF_MODE_TX 1
#define NRF_MODE_RX 2
#define NRF_MODE_PWRDOWN 3

#define nrf_CE_lo NRF_PORT&=~(1<<NRF_CE);
#define nrf_CE_hi NRF_PORT|=1<<NRF_CE;
#define nrf_CSN_lo NRF_PORT&=~(1<<NRF_CSN);
#define nrf_CSN_hi NRF_PORT|=(1<<NRF_CSN);



void nrf_init(uint8_t mode=NRF_MODE_RX);
void nrf_init_ex(uint8_t mode=NRF_MODE_RX);
void nrf_mode_rx();
void nrf_mode_tx();
void nrf_mode_powerdown();
void nrf_send(uint8_t *data, uint8_t len);
uint8_t nrf_send_ex(uint8_t *data, uint8_t len);
uint8_t nrf_send_ex(uint8_t *data);
uint8_t nrf_data_ready();
void nrf_get_data(uint8_t *data,  uint8_t len, uint8_t *p_num=0);
uint8_t nrf_get_data_ex(uint8_t * data, uint8_t *p_num=0);
void nrf_en_aa_on();
void nrf_en_aa_off();
void nrf_dynamic_payload_on();
void nrf_dynamic_payload_off();
void nrf_read_bytes(uint8_t reg, uint8_t * value, uint8_t len);
uint8_t nrf_read_byte(uint8_t reg);
void nrf_write_bytes(uint8_t reg, uint8_t * value, uint8_t len);
void nrf_write_byte(uint8_t reg, uint8_t value);
uint8_t nrf_get_status();
uint8_t nrf_get_config();
void nrf_set_addr_rx(uint8_t * addr);
void nrf_set_addr_tx(uint8_t * addr);
void nrf_set_ch(uint8_t ch);
void nrf_set_retr( uint8_t count, uint8_t delay);
void nrf_set_speed(uint8_t spd);
void nrf_enable_pipes( uint8_t flag );
void nrf_set_addr_rx_p1(uint8_t * addr);
void nrf_set_addr_rx_pipe(uint8_t p_num, uint8_t addr);

#endif
