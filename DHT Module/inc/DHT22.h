#ifndef _LIB_DHT22_H_
#define _LIB_DHT22_H_

#include <avr/io.h>
#include <util/delay.h>


#define DHT_PORT PORTA
#define DHT_DDR  DDRA
#define DHT_PIN PINA
#define DHT_DP  7

#define DHT_NOT_FOUND 2
#define DHT_SUCCESS   1
#define DHT_CRC_ERROR 0

#define DHT_lo DHT_PORT&=~(1<<DHT_DP)
#define DHT_hi DHT_PORT|=(1<<DHT_DP)

#define DHT_in  DHT_DDR&=~(1<<DHT_DP)
#define DHT_out DHT_DDR|=1<<DHT_DP

void dht_init(){
	DHT_hi; DHT_out;
}

/*	 Return values
	0, 1 - parity bit
	2 - can't find device
  temp&(1<<15) - sign of temperature (1=='-')   */
uint8_t dht_read(int *temp, int *hum){
	uint8_t data[5]={0,0,0,0,0}, parity, i,j;

	DHT_lo;
	_delay_ms(1);
	DHT_hi;

	DHT_in;
	_delay_us(45);

	if (bitIsHi(DHT_PIN, DHT_DP)) return 2;

	while (bitIsLo(DHT_PIN, DHT_DP)) {}
	while (bitIsHi(DHT_PIN, DHT_DP)) {}

	for (j=0; j<5; j++){
		for (i=0; i<8; i++){
			while (bitIsLo(DHT_PIN, DHT_DP)) {}
			_delay_us(40);
			if (bitIsHi(DHT_PIN, DHT_DP)){
				data[j]|=1<<(7-i);
				while (bitIsHi(DHT_PIN, DHT_DP)) {}
			}
		}
	}

	parity=0; for (i=0; i<4; i++) { parity+=data[i]; }
	(parity==data[4])? parity=1 : parity=0;

	*temp=0; *temp=(data[3]|(data[2]<<8));
	*hum=0; *hum=data[1]|(data[0]<<8);

	dht_init();
	return parity;
}


#endif
