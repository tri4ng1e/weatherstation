/*
		libSPI.h - header
	Author:	Sky-WaLkeR
	
	Version: 0.9
	   Date: 20.06.2013 18:26

	Description:
  This library allows you to use SPI on devices without hardware module. 
  You can use it as secondSPI on device.

	Depends:
	bit.h (can find at my BitBucket repository)

	Notes:
  This file must be in $(project_folder)\inc folder (like "spi_test\inc\libSPI.h")
  File libSPI.cpp must be in $(project_folder)\lib folder (like "spi_test\lib\libSPI.cpp")
  If you don't like this - change path in libSPI.cpp file

  WARNING! This library not tested well...
  
*/

#ifndef _LIB_SPI_PROGRAM_H_
#define _LIB_SPI_PROGRAM_H_

#include "bit.h"
#include <avr/io.h>

// pin settings
#define SPI_PORT PORTA
#define SPI_PIN  PINA
#define SPI_DDR  DDRA
#define SCK  4
#define MISO 5
#define MOSI 6

#define SCK_hi bitHi(SPI_PORT, SCK)
#define SCK_lo bitLo(SPI_PORT, SCK)
#define SCK_pulse SCK_hi;SCK_lo

#define OUT_hi bitHi(SPI_PORT, MOSI)
#define OUT_lo bitLo(SPI_PORT, MOSI)
#define IN_get bitGet(SPI_PIN, MISO)


void spi_init();

// send 1 byte and receive 1 byte
uint8_t spi_fast_shift(uint8_t send);

// send array of bytes
void spi_transmit_sync(uint8_t * dataout, uint8_t len);

// send and receive array of bytes
void spi_transfer_sync(uint8_t * dataout, uint8_t * datain, uint8_t len);

#endif
