/*
		main file
	   ������: WeatherStation
	���������: dht_new
		�����: Sky-WaLkeR
	
	   ������: 1.3
		�����: 18:40 09.08.2013
		�����: ATtiny24V @ 1 ���

	 ��������:
  ��������� ��� DHT-������. ���� ��������� �����, ����� ���������� ������
  � �������� ������ ��������� ������. ���������� Watchdog ��� ��������
  ������, ��� ���� �� ������ � �����, � ������ �������� ����������.

  ������ payload:
	������: 5 ����
	��������� ������:
   | 0 | 1 | 2 | 3 | 4 | - �����
   |ADR|Tsg|Tva|HUM|STA| - ��������

   ADR - ����� ����������� (������ WS_DHT)
   Tsg - ���� ����������� (0 == '+', 1 == '-')
   Tva - ����������� �� ������ �������� �����������
   HUM - ����������� �� ������ �������� ���������
   STA - ������ ������, ���� 7-�� ��� ����� 1 - ������� �������
    0 = ������ ������ (������ ����������)
    1 = ��� ��
    2 = ������ �� ������

*/
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "inc/nrf.h"
#include "inc/DHT22.h"

#define WS_MAIN 0
#define WS_DHT  1
#define WS_ETH  2

// ������������ ������������
#define LED_PORT PORTA
#define LED_DDR  DDRA
#define LED1     1
#define LED2     2

#define LBON_PORT	PORTB
#define LBON_DDR	DDRB
#define LBON_PIN	PINB
#define LBON		2

// ������� ��� �����������
#define LED1_off	LED_PORT|=1<<LED1
#define LED2_off	LED_PORT|=1<<LED2
#define LED1_on		LED_PORT&=~(1<<LED1)
#define LED2_on		LED_PORT&=~(1<<LED2)

#define DHT_on  PORTA|=1<<3;
#define DHT_off PORTA&=~(1<<3);

uint8_t addr_p0[5] = { 0xAB, 0xBC, 0xCD, 0xDE, 0xEF };

uint8_t data[5] = {WS_DHT,  0,   0,   0,   0 };


void send_event(){
	uint8_t retDHT=0, retNRF=0; // ��� ��������� ������
	int temp=0, hum=0;

	// �������� DHT, ���� ��� ����� � ���������, ���������
	DHT_on;
	dht_init();
	_delay_ms(2000);
	retDHT=dht_read(&temp, &hum);
	DHT_off;

	// ���������
	data[1]=(temp&(1<<15))? 1 : 0 ; // ����, 1 == '-'
	temp&=~(1<<15);
	data[2]=temp/10 + (temp%10>5? 1 : 0);
	data[3]=hum/10 + (hum%10>5? 1 : 0);
	data[4]=retDHT;
	if (bitGet(LBON_PIN, LBON)==1) data[4]|=0<<7;

	// ����������
	nrf_mode_tx();
	retNRF=nrf_send_ex(data, 5);
	_delay_ms(10);

	// �������
	if (retNRF==1 && retDHT==1){ // ��� ������ - ���� �������
		LED1_on; _delay_ms(100); LED1_off; _delay_ms(100);
	} else if (retDHT!=1 && retNRF==1) { // DHT ���� - ���� �������
		LED2_on; _delay_ms(100); LED2_off; _delay_ms(100);
	} else if (retDHT==1 && retNRF!=1) { // nRF ���� - ��� ����� �������
		LED2_on; _delay_ms(100); LED2_off; _delay_ms(100);
		LED2_on; _delay_ms(100); LED2_off; _delay_ms(100);
	} else { // ����� ���� - ��� ������� �����
		LED2_on; _delay_ms(100); LED2_off; _delay_ms(100);
		LED2_on; _delay_ms(100); LED2_off; _delay_ms(100);
		LED2_on; _delay_ms(100); LED2_off; _delay_ms(100);
	}

	// �������, ������� ��� �������� ��� ����������
	DHT_off;
	LED1_off; LED2_off;
	nrf_mode_powerdown();
}

// ���������� WDT
uint8_t wdt_isr_cnt=0;
ISR(WATCHDOG_vect){
	WDTCSR = 1<<WDIE | 1<<WDE | 1<<WDP3 | 1<<WDP0;
	if (wdt_isr_cnt>=18){ // ����� 3 �����
		wdt_isr_cnt=0;
		send_event(); // �������� ����
	} else {
		wdt_isr_cnt++; // ���� ������
	}
}


int main(){
	// ������������� ���� ��
	PORTB=0; DDRB=0xFF;
	PORTA=0; DDRA=0xFF;
	LED1_off; LED2_off;
	bitClear(LBON_DDR, LBON);
	bitSet(LBON_PORT, LBON);

	spi_init();
	nrf_init_ex();

	nrf_set_addr_tx(addr_p0);
	nrf_set_addr_rx(addr_p0);
	nrf_set_ch(38);
	nrf_set_retr(10, _1500uS);

	LED1_on;
	_delay_ms(1500);
	LED1_off;

	_delay_ms(500);

	send_event();

	// �������� �������, USI � ADC
	PRR = 1<<PRTIM1 | 1<<PRTIM0 | 1<<PRUSI | 1<<PRADC;
	// �������� WDT � ������ prescaler �� 1024K (~8 ���)
	WDTCSR = 1<<WDIE | 1<<WDE | 1<<WDP3 | 1<<WDP0;

	sei();
	while (1) {
		MCUCR = 1<<SE | 1<<SM1; // ��������� ���, ������ ����� Power-down
		asm("sleep"); // zZzzZzzZZzZzzZ
	}
	return 0;	
}
