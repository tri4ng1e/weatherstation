/*
		shift.cpp
	Project: !atm328-ws_DEBUG_main
	 Author: Sky-WaLkeR

	For information go to header file - shift.h
  
*/
#include "../inc/shift.h"

#include <avr/io.h>
#include "../inc/bit.h"
#include "../inc/libSPI.h"

void __arr_reverse(uint8_t word[], uint8_t len){
    for (int i=0;i<len/2;i++) {
    	word[i]^=word[len-i-1];
    	word[len-i-1]^=word[i];
    	word[i]^=word[len-i-1];
    }
}

uint8_t shift_arr[SH_SIZE]; // main data array
uchar __arr_t[8]={SH_T_11, SH_T_12, SH_T_21, SH_T_22, SH_T_31, SH_T_32, SH_T_41, SH_T_42};
uchar __arr_c[8]={SH_C_11, SH_C_12, SH_C_13, SH_C_14, SH_C_21, SH_C_22, SH_C_23, SH_C_24};

// INTERNAL USE ONLY - converts digit into shift mask
uchar __dig2shift(uchar dig){
	switch(dig){
	case 0: return SH_DIG_0; break;
	case 1: return SH_DIG_1; break;
	case 2: return SH_DIG_2; break;
	case 3: return SH_DIG_3; break;
	case 4: return SH_DIG_4; break;
	case 5: return SH_DIG_5; break;
	case 6: return SH_DIG_6; break;
	case 7: return SH_DIG_7; break;
	case 8: return SH_DIG_8; break;
	case 9: return SH_DIG_9; break;
	}
	return 0;
}

// INTERNAL USE ONLY - returns absolute value of number (w\o sign)
uchar __abs(schar a){
	if (a>0) return a; else return -a;
}

/* FUNCTION shift_init( flag )
	Do all thing neede for correct shiftReg work
 */
void shift_init(){

	bitHi( SH_RST_PORT, SH_RST_PIN );
	SH_LOCK_PORT|=1<<SH_LOCK_PIN;
	spi_init();
	shift_arr_reset();
	shift_out();
}
/* FUNCTION shift_out()
	Clocks shift_arr to shiftReg using SPI
 */
void shift_out(){
	SH_LOCK_PORT&=~(1<<SH_LOCK_PIN);
	SH_RST_PORT&=~(1<<SH_RST_PIN);
	SH_RST_PORT|=1<<SH_RST_PIN;
	__arr_reverse(shift_arr, SH_SIZE);
	spi_transmit_sync(shift_arr, SH_SIZE);
	SH_LOCK_PORT|=1<<SH_LOCK_PIN;
	__arr_reverse(shift_arr, SH_SIZE);
	SH_LOCK_PORT&=~(1<<SH_LOCK_PIN);
}

void shift_arr_reset(){
	for (uchar i=0; i<SH_SIZE; i++){
		shift_arr[i]=0;
	}
}

void shift_arr_set_temp(signed char t1, signed char t2, signed char t3, signed char t4){
	signed char *ptrs[4]={&t1, &t2, &t3, &t4};
	shift_arr[SH_MINUS]&=~(1<<SH_MINUS_1 | 1 << SH_MINUS_2 | 1<< SH_MINUS_3 | 1 << SH_MINUS_4);

	for (uint8_t i=0; i<4; i++){
		if (*ptrs[i]==SH_NOPE){
			shift_arr[__arr_t[i*2]]=1<<SH_G;
			shift_arr[__arr_t[i*2]+1]=1<<SH_G;
		} else {
			if (__abs(*ptrs[i])>10) {
				shift_arr[__arr_t[i*2]]=__dig2shift(__abs(*ptrs[i]/10));
			} else {
				shift_arr[__arr_t[i*2]]=0;
			}
			shift_arr[__arr_t[i*2+1]]=__dig2shift(__abs(*ptrs[i]%10));
		}
	}
	if (t1<0) shift_arr[SH_MINUS]|=1<<SH_MINUS_1;
	if (t2<0) shift_arr[SH_MINUS]|=1<<SH_MINUS_2;
	if (t3<0) shift_arr[SH_MINUS]|=1<<SH_MINUS_3;
	if (t4<0) shift_arr[SH_MINUS]|=1<<SH_MINUS_4;
}

void shift_arr_set_day(uchar day){
	uchar b=0;
	switch (day){
	case Mon: b=1<<SH_MON; break;
	case Tue: b=1<<SH_TUE; break;
	case Wed: b=1<<SH_WED; break;
	case Thu: b=1<<SH_THU; break;
	case Fri: b=1<<SH_FRI; break;
	case Sat: b=1<<SH_SAT; break;
	case Sun: b=1<<SH_SUN; break;
	}
	shift_arr[SH_DAYS]=b;
}

void shift_arr_set_cond(uchar c1, uchar c2, uchar c3, uchar c4){
	shift_arr[SH_COND1]=(c1!=SH_NOPE? 1 : 0) <<__arr_c[c1-1] | (c2!=SH_NOPE? 1 : 0) <<__arr_c[c2+3];
	shift_arr[SH_COND2]=(c3!=SH_NOPE? 1 : 0) <<__arr_c[c3-1] | (c4!=SH_NOPE? 1 : 0) <<__arr_c[c4+3];
}

void shift_arr_set_date(uchar day, uchar mon, uchar year){
	shift_arr[SH_DATE1]=__dig2shift(day/10);
	shift_arr[SH_DATE2]=__dig2shift(day%10)|1<<SH_DOT;
	shift_arr[SH_DATE3]=__dig2shift(mon/10);
	shift_arr[SH_DATE4]=__dig2shift(mon%10)|1<<SH_DOT;
	shift_arr[SH_DATE5]=__dig2shift(year/10);
	shift_arr[SH_DATE6]=__dig2shift(year%10);
}

void shift_arr_set_dht(signed char temp, uint8_t hum){
	shift_arr[SH_DATE3]=SH_DIG_grad;
	shift_arr[SH_DATE6]=SH_DIG_h;

	if (temp>100 || hum>100) {
		shift_arr[SH_DATE1]=1<<SH_G;
		shift_arr[SH_DATE2]=1<<SH_G;
		shift_arr[SH_DATE4]=1<<SH_G;
		shift_arr[SH_DATE5]=1<<SH_G;
		shift_arr[SH_MINUS]&=~(1<<SH_MINUS_D);
		return;
	}
	(temp<0)? shift_arr[SH_MINUS]|=1<<SH_MINUS_D : shift_arr[SH_MINUS]&=~(1<<SH_MINUS_D);
	shift_arr[SH_DATE2]=__dig2shift(__abs(temp%10));
	if (__abs(temp)>10) {
		shift_arr[SH_DATE1]=__dig2shift(__abs(temp/10));
	} else {
		shift_arr[SH_DATE1]=0;
	}
	shift_arr[SH_DATE5]=__dig2shift(hum%10);
	if (hum>10) {
		shift_arr[SH_DATE4]=__dig2shift(hum/10);
	}
}

void shift_arr_set_dht_err(){
	shift_arr[SH_DATE1]=SH_DIG_e;
	shift_arr[SH_DATE2]=SH_DIG_r;
	shift_arr[SH_DATE3]=SH_DIG_r;
	shift_arr[SH_DATE4]=SH_DIG_o;
	shift_arr[SH_DATE5]=SH_DIG_r;
	shift_arr[SH_DATE6]=0;
	shift_arr[SH_MINUS]&=~(1<<SH_MINUS_D);
}

void shift_arr_set_dht_lo_ba(){
	shift_arr[SH_DATE1]=SH_DIG_L;
	shift_arr[SH_DATE2]=SH_DIG_o;
	shift_arr[SH_DATE3]=0;
	shift_arr[SH_DATE4]=SH_DIG_b;
	shift_arr[SH_DATE5]=SH_DIG_A;
	shift_arr[SH_DATE6]=0;
	shift_arr[SH_MINUS]&=~(1<<SH_MINUS_D);
}
