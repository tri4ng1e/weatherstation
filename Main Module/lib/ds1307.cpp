#include "../inc/ds1307.h"

uint8_t bcd2bin(uint8_t x){
    return (((x & 0xF0)>>4)*10 + (x & 0x0F));
}

uint8_t bin2bcd(uint8_t x){
    return ((x%10) | ((x/10)<<4));
}

void rtc_init(uint8_t PeriodSelect, uint8_t SQWe, uint8_t OUTlevel){
    PeriodSelect&=3;
    if (SQWe) PeriodSelect |= 0x10;
    if (OUTlevel) PeriodSelect |= 0x80;
    i2c_init();
    i2c_start(DS1307_ADDR);
    i2c_write(CONTROL);
    i2c_write(PeriodSelect);
    i2c_stop();
}

void rtc_get_time(uint8_t *hour, uint8_t *min, uint8_t *sec){
    i2c_start(DS1307_ADDR);
    i2c_write(0);
    i2c_start(DS1307_ADDR, I2C_READ);
    *sec = bcd2bin(i2c_read()) & 0x7f;
    *min = bcd2bin(i2c_read());
    *hour = bcd2bin(i2c_read(I2C_NAK));
    i2c_stop();
}

void rtc_set_time(uint8_t hour, uint8_t min, uint8_t sec){
    i2c_start(DS1307_ADDR);
    i2c_write(0);
    i2c_write(bin2bcd(sec));
    i2c_write(bin2bcd(min));
    i2c_write(bin2bcd(hour));
    i2c_stop();
}

// legacy - for old projects
void rtc_get_date(uint8_t *date, uint8_t *month, uint8_t *year){
    i2c_start(DS1307_ADDR);
    i2c_write(4);
    i2c_start(DS1307_ADDR, I2C_READ);
    *date=bcd2bin(i2c_read());
    *month=bcd2bin(i2c_read());
    *year=bcd2bin(i2c_read(I2C_NAK));
    i2c_stop();
}

void rtc_get_date(uint8_t *day_week, uint8_t *date, uint8_t *month, uint8_t *year){
    i2c_start(DS1307_ADDR);
    i2c_write(3);
    i2c_start(DS1307_ADDR, I2C_READ);
    *day_week=i2c_read();
    *date=bcd2bin(i2c_read());
    *month=bcd2bin(i2c_read());
    *year=bcd2bin(i2c_read(I2C_NAK));
    i2c_stop();
}

void rtc_set_date(uint8_t date, uint8_t month, uint8_t year){
    i2c_start(DS1307_ADDR);
    i2c_write(4);
    i2c_write(bin2bcd(date));
    i2c_write(bin2bcd(month));
    i2c_write(bin2bcd(year));
    i2c_stop();
}

void rtc_start(uint8_t flag){
	i2c_start(DS1307_ADDR);
	i2c_write(SECONDS);
	i2c_start(DS1307_ADDR, I2C_READ);
	uint8_t conf=i2c_read(I2C_NAK);
	i2c_start(DS1307_ADDR);
	i2c_write(SECONDS);
	i2c_write(conf&(0b1111111|(!flag<<7)));
	i2c_stop();
}
uint8_t rtc_is_started(){
	i2c_start(DS1307_ADDR);
	i2c_write(SECONDS);
	i2c_start(DS1307_ADDR, I2C_READ);
	uint8_t conf=i2c_read(I2C_NAK);
	i2c_stop();
	return bit_is_clear(conf, 7);
}


void rtc_hour_mode(uint8_t mode){
	i2c_start(DS1307_ADDR);
	i2c_write(HOURS);
	i2c_start(DS1307_ADDR, I2C_READ);
	uint8_t conf=i2c_read(I2C_NAK);
	i2c_start(DS1307_ADDR);
	i2c_write(HOURS);
	i2c_write(conf&(0b1111111|(mode<<6)));
	i2c_stop();
}

void rtc_get_day(uint8_t *day){
	i2c_start(DS1307_ADDR);
	i2c_write(DAY);
	i2c_start(DS1307_ADDR, I2C_READ);
	*day = bcd2bin(i2c_read());
	i2c_stop();
}
void rtc_set_day(uint8_t day){
    i2c_start(DS1307_ADDR);
    i2c_write(DAY);
    i2c_write(bin2bcd(day));
    i2c_stop();
}

void rtc_get_all(uint8_t *arr){
	i2c_start(DS1307_ADDR);
	i2c_write(0);
	i2c_start(DS1307_ADDR, I2C_READ);
	arr[0] = bcd2bin(i2c_read()) & 0x7f;
	arr[1] = bcd2bin(i2c_read());
	arr[2] = bcd2bin(i2c_read()) & 0x3f;
	arr[3] = i2c_read();
	arr[4] = bcd2bin(i2c_read());
	arr[5] = bcd2bin(i2c_read());
	arr[6] = bcd2bin(i2c_read());
	i2c_stop();
}
