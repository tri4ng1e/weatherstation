/*
    Copyright (c) 2007 Stefan Engelke <mbox@stefanengelke.de>
	Modified by Sky-WaLkeR, May 2013 <alex@naboko.tk>

*/

//#include <myLibs/libSPI.h>

#include <avr/io.h>

void spi_init(){
    SPCR = ((1<<SPE)|               // SPI Enable
            (0<<SPIE)|              // SPI Interupt Enable
            (0<<DORD)|              // Data Order (0:MSB first / 1:LSB first)
            (1<<MSTR)|              // Master/Slave select   
            (0<<SPR1)|(1<<SPR0)|    // SPI Clock Rate
            (0<<CPOL)|              // Clock Polarity (0:SCK low / 1:SCK hi when idle)
            (0<<CPHA));             // Clock Phase (0:leading / 1:trailing edge sampling)
    SPSR = (1<<SPI2X);              // Double Clock Rate
}

// Shift full array through target device
void spi_transfer_sync (uint8_t * dataout, uint8_t * datain, uint8_t len){
	uint8_t i;
	for (i = 0; i < len; i++) {
		SPDR = dataout[i];
		while((SPSR & (1<<SPIF))==0);
		datain[i] = SPDR;
	}
}

// Shift full array to target device without receiving any byte
void spi_transmit_sync (unsigned char *dataout, unsigned char len){
	uint8_t i;
	for (i = 0; i < len; i++) {
		SPDR = dataout[i];
		while((SPSR & (1<<SPIF))==0);
	}
}

// Clocks one byte to target device and returns the received one
uint8_t spi_fast_shift (uint8_t data){
    SPDR = data;
    while((SPSR & (1<<SPIF))==0);
    return SPDR;
}
