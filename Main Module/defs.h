#ifndef _H_defs_INC_
#define _H_defs_INC_


MAKE_PORT(PORTA, DDRA, PINA, Porta, 'A');
MAKE_PORT(PORTB, DDRB, PINB, Portb, 'B');
MAKE_PORT(PORTC, DDRC, PINC, Portc, 'C');
MAKE_PORT(PORTD, DDRD, PIND, Portd, 'D');

// ��������� ��� ���� ���������� �������� \ ����������
struct Globals{
	signed char temp[4], dht; uint8_t dhtErr;
	uint8_t hour, min, sec, day, mon, year, dayWeek, hum, cond[4], pwm;
	uint8_t fl_pwm_ro, nrf_dht_lost; uint16_t nrf_dht_cnt;
	uint8_t nrf_ch, nrf_addr_rx[5], adc_read;
};


// ����� ����� �������� ��� ������������ �����
#define DYN_A   3
#define DYN_B   5
#define DYN_C   0
#define DYN_D   2
#define DYN_E   7
#define DYN_F   6
#define DYN_G   4
#define DYN_DOT 1

// ����� ������ ��������
typedef TPin< Portb, 4> DYN_C1_EN;
typedef TPin< Porta, 6> DYN_C2_EN;
typedef TPin< Porta, 5> DYN_C3_EN;
typedef TPin< Porta, 4> DYN_C4_EN;

// ���� SQ
typedef TPin< Portc, 5> RTC_SQ;

// �������, ������� �������
typedef TPin< Portd, 3> PROG_IN;
typedef TPin< Portd, 5> PROG_OUT;

// ����� ���
#define ADC_CH 7

// �������������� �������
#define WS_MAIN 0
#define WS_DHT  1
#define WS_ETH  2
#define WS_DEBUG 5

// ���������� �������
/// �������
void parse_brightness();
void parse_rtc_time();
void parse_rtc_date(uint8_t update);
void parse_mirf();
void parse_PROGjumper();
void date_event();
/// ������
void timer2_init();



#endif
