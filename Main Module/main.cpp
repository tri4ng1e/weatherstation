/*
		main file
	   ������: WeatherStation
	���������: MAIN uncomp v4
		�����: Sky-WaLkeR

	   ������: 4.0.17
		�����: 22:55 09.08.2013
		�����: ATmega32 @ 8 ���

	 ��������:
  ��������� ��� ��������� ������

*/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "inc/port.h"
#include "defs.h"

#include "inc/libTWI.h"
#include "inc/ds1307.h"
#include "inc/nrf.h"

#include "inc/shift.h"
#include "inc/v_port.h" // ����������� ����
#include "inc/dyn.h" // ��� ������������ ���������
#include "inc/adc.h" // ������ �����

// ��� ����� ���������� ���������� � ���������
Globals global;

uint8_t  main_addr[5] = {0xAB, 0xBC, 0xCD, 0xDE, 0xEF};
uint8_t   eth_addr[5] = {0x02, 0xCB, 0xDC, 0xED, 0xFE};
uint8_t debug_addr[5] = {0x05, 0xCB, 0xDC, 0xED, 0xFE};
uint8_t payload[32];


// ����� ��������, ����� ����, ��������� ������� �� 3
void parse_PROGjumper(){
	if (PROG_IN::IsSet()==0){
		global.fl_pwm_ro=1;
		global.pwm=3;
		dyn_set_pwm(global.pwm);
	} else {
		global.fl_pwm_ro=0;
	}
	_delay_ms(1);
	shift_out();
}
// ���������� �� �������
ISR(INT1_vect){
	parse_PROGjumper();
}

// ���������� ������� � RTC
void parse_rtc_time(){
	rtc_get_time(&global.hour, &global.min, &global.sec);
	dyn_dig1=global.hour/10;
	dyn_dig2=global.hour%10;
	dyn_dig3=global.min/10;
	dyn_dig4=global.min%10;
}
// ���������� ���� � RTC
void parse_rtc_date(uint8_t update=1){
	rtc_get_date(&global.dayWeek, &global.day, &global.mon, &global.year);
	shift_arr_set_day(global.dayWeek);
	shift_arr_set_date(global.day, global.mon, global.year);
	if (update==1) shift_out();
}

// ����� ������� �������
void parse_brightness(){
	adc_start_wait(ADC_CH);
	global.adc_read = adc_read();
	adc_stop();
	if(global.fl_pwm_ro==0) {
		global.pwm += ( global.adc_read - global.pwm )/8;
		if (global.pwm<3) global.pwm=3;
		if (global.pwm>215) global.pwm=215;

	}
	dyn_set_pwm(global.pwm);
}

// ����� ���������
void parse_nrf(){
	if (nrf_data_ready()){ // ���-�� ������?
		nrf_get_data_ex(payload);
		switch (payload[0]){ // �� ����?
		case WS_DHT: // �� DHT
			global.nrf_dht_cnt=0; global.nrf_dht_lost=0;
			if ((payload[4]&0x7f)==4){
				global.dhtErr=1; // Error
			} else if (((payload[4] & 1<< 7 )>> 7) == 1){
				global.dhtErr=2; // Lo Bat
			} else { // ��� ������
				global.dhtErr=0;
				global.dht=payload[2] * ((payload[1]==1)? -1 : 1);
				global.hum=payload[3];
			}
			break;
		case WS_ETH: // �� ETH
			global.hour=payload[1];
			global.min=payload[2];
			global.sec=payload[3];
			global.day=payload[4];
			global.mon=payload[5];
			global.year=payload[6];
			global.dayWeek = payload[7]==0? 7 : payload[7];

			rtc_set_time(global.hour, global.min, global.sec);
			rtc_set_date(global.day, global.mon, global.year);
			rtc_set_day(global.dayWeek);

			global.temp[0]= (payload[8]>>7)==1? -payload[8] : payload[8];
			global.temp[1]= (payload[9]>>7)==1? -payload[9] : payload[9];
			global.temp[2]= (payload[10]>>7)==1? -payload[10] : payload[10];
			global.temp[3]= (payload[11]>>7)==1? -payload[11] : payload[11];

			global.cond[0]=payload[12];
			global.cond[1]=payload[13];
			global.cond[2]=payload[14];
			global.cond[3]=payload[15];

			shift_arr_set_cond(global.cond[0], global.cond[1], global.cond[2], global.cond[3]);
			shift_arr_set_temp(global.temp[0], global.temp[1], global.temp[2], global.temp[3]);

			break;
		} // case end
		shift_out(); // ���������
	}

	// ���� DHT �� ��������� �� ��� �����
	if ((global.nrf_dht_cnt > 4*60) && global.nrf_dht_lost!=1){
		global.dhtErr=0;
		global.dht=255; global.hum=255;
		global.nrf_dht_lost=1;
		shift_out();
	}
	global.nrf_dht_cnt++;
}

// ����� ���� � nRF
uint8_t date_state=0;
void date_event(){
	date_state=!date_state;
	if (date_state==0){ // ���������� ����
		shift_arr_set_date(global.day, global.mon, global.year);
	} else { // ���������� nRF
		switch (global.dhtErr){
		case 1: // DHT-������ �� ������
			shift_arr_set_dht_err(); break;
		case 2: // ��������� �������
			shift_arr_set_dht_lo_ba(); break;
		default: // ��� ���������
			shift_arr_set_dht(global.dht, global.hum); break;
		}
	}
	shift_out(); // ����������
}

// ������ 2.5 �� - ������, ��� ���������� (������ �� �����)
uint16_t tim2_cnt=0;
void timer2_init(){
	TCNT2=177; // ��������� ���������
	TCCR2=6; // ������������
}


int main(){
	DDRA=0x7f; DDRB=0xff;
	DDRC=0b11011111; DDRD=0xff;
	PORTA=0x00; PORTB=0x00;
	PORTC=1<<5; PORTD=0x00;

	// PROG-������� - ���� ���� �����-HIGH, ������ ����
	PROG_IN::Set();
	PROG_IN::SetDirRead();
	PROG_OUT::SetDirWrite();
	PROG_OUT::Clear();

	// ��������� ���������� ����������
	global.nrf_ch=38;

	global.nrf_dht_lost=0;
	global.nrf_dht_cnt=4*60+1;
	global.dht=255; global.hum=255;

	_delay_ms(50); // �� ����� :)

	shift_init(); // ������������� ���� ��
	adc_init();
	dyn_init();
	rtc_init(0, 1, 1);

	nrf_init_ex();
	nrf_set_addr_rx(main_addr);
	nrf_set_addr_tx(eth_addr);
	nrf_set_ch(global.nrf_ch);
	nrf_set_retr(10, _1500uS);

	parse_PROGjumper(); // PROG-�������, ������ �������� (�.�. ���������� �� ���������)

	// ��������� ���������� �� PROG-��������
	MCUCR|=1<<ISC10;
	GICR|=1<<INT1;
	sei();

	// �������� ��� �� �������
	parse_brightness();
	//dyn_set_pwm(6);
	dyn_pwm(1);

	_delay_ms(50);
//	rtc_start(); // ���� �� ������ RTC

//	  say HELLO )
	shift_arr[SH_DATE1]=1<<SH_B|1<<SH_C|1<<SH_E|1<<SH_F|1<<SH_G;
	shift_arr[SH_DATE2]=1<<SH_A|1<<SH_D|1<<SH_E|1<<SH_F|1<<SH_G;
	shift_arr[SH_DATE3]=1<<SH_D|1<<SH_E|1<<SH_F;
	shift_arr[SH_DATE4]=1<<SH_D|1<<SH_E|1<<SH_F;
	shift_arr[SH_DATE5]=__dig2shift(0);
	shift_out();
	_delay_ms(1500);

	// ��������� ������ ���������
	global.temp[0]=global.temp[1]=global.temp[2]=global.temp[3]=SH_NOPE;
	global.cond[0]=global.cond[1]=global.cond[2]=global.cond[3]=SH_NOPE;

	// �������
	shift_arr_set_cond(global.cond[0], global.cond[1], global.cond[2], global.cond[3]);
	shift_arr_set_temp(global.temp[0], global.temp[1], global.temp[2], global.temp[3]);

	// �������� �����
	parse_rtc_time();
	parse_rtc_date();

	_delay_ms(10);

	tim2_cnt=0;
	timer2_init();

	while (1){
		dyn_event(); // ����������� ��������

		if (tim2_cnt % 51 == 0){ // 8 ��� � �������
			parse_rtc_time();
			if (global.hour+global.min+global.sec == 0) { parse_rtc_date(); }
		}
		if (tim2_cnt % 20 == 0){ // ~ 20 ��� � �������
			parse_brightness();
		}
		if (tim2_cnt % 199 == 0){ // ~ 2 ���� � �������
			parse_nrf();
		}

		if (tim2_cnt == 1999) { // 5 ������
			date_event();
		}

		tim2_cnt==2000? tim2_cnt=0 : tim2_cnt++; // ������� �������

		while ((TIFR & 1<<TOV2)>>TOV2 == 0) {} // ���� ������������
		TIFR|=1<<TOV2; // �������
		cli(); TCNT2=177; sei(); // ���������
	}
	return 0;	
}
