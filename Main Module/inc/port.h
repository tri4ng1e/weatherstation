/*
		port.h - header
	   ������: WeatherStation
	���������: MAIN uncomp v4
		�����: Neiver (wrapped by Sky-WaLkeR)
	
	   ������: 1.0

	 ��������:
  ���������� ��������� ������ �������� � ������� �����-������ � ������.
  
  ������� Neiver @ easyelectronics.ru - 90% ������� ��, � �������� � ����������
  � ������� ������������, ��, ������� ���� �������...

	�������������:
  ������� ����� ������� ������ MAKE_PORT ( PORTx, DDRx, PINx, name, ID), ���:
  	  PORTx, DDRx, PINx - ��������� �����, �������� PORTB, DDRB, PINB
  	  name - ��� �������� ������
  	  ID - ���������� �������������
  ������ ����� �������� � ���� ������:
    name::Write ( data ) - ������ � ���� ( ���������� PORTx=data )
    name::Read () - ������ ( ���������� data=PORTx )
    name::PinRead() - ������ � �������� PINx
    name::ClearAndSet ( clearMask, value ) - �������� ���� clearMask � ���������� value (?)
    name::DirWrite ( data ) - ������  ������� ����������� ( ���������� DDRx=data )
    name::DirRead () - ����������� =)
    name::Set ( data  ) - ��������� ����� ( PORTx|=data )
    name::Clear ( data ) - ������� ����� ( like PORTx&=~(data) )
    name::Toggle ( data ) - ��������� �����, ����������� ��� ( PORTx^=value )
    Set, Clear and Toggle �������� � � Dir... ��������� - name::DirSet()
    
  ����� ���������� ��������� ����� �������� � ������:
    typedef TPin<name, x> ledPin, ���:
      name - ��� ������, ������� �� �������� � MAKE_PORT
      x - ����� ����
      ledPin - ������� ��� ������ ��� ����
  ������ ����� ����� ������ ��� ���:
    ledPin::Set() - ���������� � HIGH
    ledPin::Clear() - �������� � LOW
	ledPin::getPort() - ������� ���� (PORTx)
	ledPin::getDdr() - ������� ������� ����������� (DDRx)
	ledPin::getMask() - ������� ���-����� (���� ����� ���� 3, ������ 0b1000)
    ����� ��������: Toggle, IsSet, SetDir, SetDirWrite, SetDirRead, SetDir(mode)
    �����, ��� ��� �������... ���� mode!=0, ��� � ������ output, � ��������
  
*/
#ifndef _LIB_PORT_H_
#define _LIB_PORT_H_

#define MAKE_PORT(portName, ddrName, pinName, className, ID) \
	class className{\
	public:\
		typedef uint8_t DataT;\
	private:\
		static volatile DataT &data(){\
			return portName;\
		}\
		static volatile DataT &dir(){\
			return ddrName;\
		}\
		static volatile DataT &pin(){\
			return pinName;\
		}\
	public:\
		static void Write(DataT value){\
			data() = value;\
		}\
		static void ClearAndSet(DataT clearMask, DataT value){\
			data() = (data() & ~clearMask) | value;\
		}\
		static DataT Read(){\
			return data();\
		}\
		static void DirWrite(DataT value){\
			dir() = value;\
		}\
		static DataT DirRead(){\
			return dir();\
		}\
		static void Set(DataT value){\
			data() |= value;\
		}\
		static void Clear(DataT value){\
			data() &= ~value;\
		}\
		static void Toggle(DataT value){\
			data() ^= value;\
		}\
		static void DirSet(DataT value){\
			dir() |= value;\
		}\
		static void DirClear(DataT value){\
			dir() &= ~value;\
		}\
		static void DirToggle(DataT value){\
			dir() ^= value;\
		}\
		static DataT PinRead(){\
			return pin();\
		}\
		enum{Id = ID};\
		enum{Width=sizeof(DataT)*8};\
	};
// END define MAKE_PORT

template<class PORT, uint8_t PIN>
	class TPin {
	public:
		typedef PORT Port;
		enum{Number = PIN};
		static void Set(){
			PORT::Set(1 << PIN);
		}
		static void Set(uint8_t val){
			if(val)
				Set();
			else Clear();
		}
		static void SetDir(uint8_t val){
			if(val)
				SetDirWrite();
			else SetDirRead();
		}
		static void Clear(){
			PORT::Clear(1 << PIN);
		}
		static void Toggle(){
			PORT::Toggle(1 << PIN);
		}
		static void SetDirRead(){
			PORT::DirClear(1 << PIN);
		}
		static void SetDirWrite(){
			PORT::DirSet(1 << PIN);
		}
		static uint8_t IsSet(){
			return (PORT::PinRead() & (uint8_t)(1 << PIN)) >> PIN;
		}
		static uint8_t getMask(){
			return (1 << PIN);
		}
		static uint8_t getPort(){
			return PORT::data();
		}
		static uint8_t getDdr(){
			return PORT::ddr();
		}
	};

#endif
