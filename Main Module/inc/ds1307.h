#ifndef _LIB_DS1307_H_
#define _LIB_DS1307_H_

#include "../inc/libTWI.h"

#ifndef _LIB_TWI_H_
#error Library DS1307 has dependency from libTWI.h. Add please
#endif

#define DS1307_ADDR 0xd0

#define SECONDS	0x00
#define MINUTES	0x01
#define HOURS	0x02
#define DAY		0x03
#define DATE	0x04
#define MONTH	0x05
#define YEAR	0x06
#define CONTROL	0x07

#define H24 0
#define H12 1


extern uint8_t bcd2bin(uint8_t x);

extern uint8_t bin2bcd(uint8_t x);

extern void rtc_init(uint8_t PeriodSelect=0,   // period on OUT
                                        // 0 - 1 Hz
                                        // 1 - 4096 Hz
                                        // 2 - 8192 Hz
                                        // 3 - 32768 Hz
              uint8_t SQWe=1,           // signal on OUT
              uint8_t OUTlevel=1        // OUT level, if SQWe==0
              );

extern void rtc_get_time(uint8_t *hour, uint8_t *min, uint8_t *sec);

extern void rtc_set_time(uint8_t hour, uint8_t min, uint8_t sec);

// legacy - for old projects
extern void rtc_get_date(uint8_t *date, uint8_t *month, uint8_t *year);

extern void rtc_get_date(uint8_t *day_week, uint8_t *date, uint8_t *month, uint8_t *year);

extern void rtc_set_date(uint8_t date, uint8_t month, uint8_t year);

extern void rtc_start(uint8_t flag=1);

extern uint8_t rtc_is_started();

extern void rtc_hour_mode(uint8_t mode=H24);

extern void rtc_get_day(uint8_t *day);

extern void rtc_set_day(uint8_t day);

extern void rtc_get_all(uint8_t *arr);

#endif
