// Defines for nrf24.h
// Author: Sky-WaLkeR

/* Memory Map */
#define CONFIG      0x00
#define EN_AA       0x01
#define EN_RXADDR   0x02
#define SETUP_AW    0x03
#define SETUP_RETR  0x04
#define RF_CH       0x05
#define RF_SETUP    0x06
#define STATUS      0x07
#define OBSERVE_TX  0x08
#define CD          0x09
#define RX_ADDR_P0  0x0A
#define RX_ADDR_P1  0x0B
#define RX_ADDR_P2  0x0C
#define RX_ADDR_P3  0x0D
#define RX_ADDR_P4  0x0E
#define RX_ADDR_P5  0x0F
#define TX_ADDR     0x10
#define RX_PW_P0    0x11
#define RX_PW_P1    0x12
#define RX_PW_P2    0x13
#define RX_PW_P3    0x14
#define RX_PW_P4    0x15
#define RX_PW_P5    0x16
#define FIFO_STATUS 0x17
#define DYNPD		0x1C
#define FEATURE		0x1D

/* Bit Mnemonics */
#define MASK_RX_DR  6
#define MASK_TX_DS  5
#define MASK_MAX_RT 4
#define EN_CRC      3
#define CRCO        2
#define PWR_UP      1
#define PRIM_RX     0
#define ENAA_P5     5
#define ENAA_P4     4
#define ENAA_P3     3
#define ENAA_P2     2
#define ENAA_P1     1
#define ENAA_P0     0
#define ERX_P5      5
#define ERX_P4      4
#define ERX_P3      3
#define ERX_P2      2
#define ERX_P1      1
#define ERX_P0      0
#define AW          0
#define ARD         4
#define ARC         0
#define PLL_LOCK    4
#define RF_DR       3
#define RF_PWR      1
#define LNA_HCURR   0        
#define RX_DR       6
#define TX_DS       5
#define MAX_RT      4
#define RX_P_NO     1
#define TX_FULL     0
#define PLOS_CNT    4
#define ARC_CNT     0
#define TX_REUSE    6
#define FIFO_FULL   5
#define TX_EMPTY    4
#define RX_FULL     1
#define RX_EMPTY    0
#define EN_DPL		2

/* Instruction Mnemonics */
#define R_REGISTER    0x00
#define W_REGISTER    0x20
#define REGISTER_MASK 0x1F
#define R_RX_PAYLOAD  0x61
#define W_TX_PAYLOAD  0xA0
#define FLUSH_TX      0xE1
#define FLUSH_RX      0xE2
#define REUSE_TX_PL   0xE3
#define R_RX_PL_WID	  0x60
#define NOP           0xFF
#define W_TX_PAYLOAD_NOACK 0xB0

/* Speed definitions */
#define SPD_250K	1
#define SPD_1M		2
#define SPD_2M		3

/* Delay definitions (for RETR) */
#define _250uS  0
#define _500uS  1
#define _750uS  2
#define _1000uS 3
#define _1250uS 4
#define _1500uS 5
#define _1750uS 6
#define _2000uS 7
#define _2250uS 8
#define _2500uS 9
#define _2750uS 10
#define _3000uS 11
#define _3250uS 12
#define _3500uS 13
#define _3750uS 14
#define _4000uS 15

/* pipe definitions (for nrf_enable_pipes only, in other funcs use numbers) */
#define PIPE_0   1
#define PIPE_1   1<<ERX_P1
#define PIPE_2   1<<ERX_P2
#define PIPE_3   1<<ERX_P3
#define PIPE_4   1<<ERX_P4
#define PIPE_5   1<<ERX_P5
#define PIPE_ALL PIPE_0|PIPE_1|PIPE_2|PIPE_3|PIPE_4|PIPE_5
#define PIPE_NONE 0

#define NRF_SEND_FAILURE  0
#define NRF_SEND_SUCCESS  1
#define NRF_SEND_TIMEOUT  2
#define NRF_SEND_TOO_LONG 3

/* Misc definiions */
#define ON  1
#define OFF 0
