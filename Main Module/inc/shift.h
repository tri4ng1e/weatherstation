/*
		shift.h - header
	Project: !atm32-ws-main_uncomp_v2
	Author:	Admin
	
	Version: 0.4
	   Date: 08.05.2013

	Description:
  This file contains byte and bit maps for shift registers, shift array and some
  functions ( they may use libSPI library )

    The main goal - create array of data for shift registers and send it via SPI.
  Pins on shift registers (ShReg) located not in linear order - thats why all ping goes as macros.

    Functions: (* - not implemented, ! - WIP, # - not tested)
  shift_init() - sets all values to defaults and init SPI module
  shift_arr_reset() - sets all values of array to 0
  shift_out() - sends array (reversed) through SPI port

  shift_arr_set_temp( Mfloat t1, Mfloat t2, Mfloat t3, Mfloat t4 ) - temp, dots and minuses
  shift_arr_set_day ( uchar day ) - day of week (1=Monday ... 7=Sunday or defines)
  shift_arr_set_cond( uchar c1, uchar c2, uchar c3, uchar c4 )
  shift_arr_set_dht ( Mfloat temp, Mfloat hum )
  shift_arr_set_date( uchar day, uchar mon, uchar year )

  
*/
#ifndef _H_defines_INC_
#define _H_defines_INC_

#include <myLibs/types.h>
#include <myLibs/bit.h>
#include <avr/io.h>

// RESET pin of shift registors
#define SH_RST_PIN   7
#define SH_RST_PORT  PORTC
#define SH_LOCK_PIN  6
#define SH_LOCK_PORT PORTC

#define SH_SIZE 18 // count of shiftReg
extern uchar shift_arr[SH_SIZE]; // main data array

// Shift Byte map
// date idicators - CORRECT
#define SH_DATE1 0
#define SH_DATE2 1
#define SH_DATE3 2
#define SH_DATE4 3
#define SH_DATE5 4
#define SH_DATE6 5

// temp indicators - CORRECT
#define SH_T_11 6
#define SH_T_12 7
#define SH_T_21 8
#define SH_T_22 9
#define SH_T_31 10
#define SH_T_32 11
#define SH_T_41 12
#define SH_T_42 13
extern uchar __arr_t[8];

// condition - CORRECT
#define SH_COND1 16
#define SH_COND2 17

// misc - CORRECT
#define SH_MINUS 14
#define SH_DAYS  15

//==================================================
// Shift Bit map
// indicator LEDs - CORRECT
#define SH_A   3
#define SH_B   1
#define SH_C   2
#define SH_D   4
#define SH_E   6
#define SH_F   5
#define SH_G   7
#define SH_DOT 0

// condition LEDs - CORRECT
#define SH_C_11 3
#define SH_C_12 2
#define SH_C_13 1
#define SH_C_14 0
#define SH_C_21 4
#define SH_C_22 5
#define SH_C_23 6
#define SH_C_24 7
extern uchar __arr_c[8];

// minus LEDs - CORRECT
#define SH_MINUS_1 4
#define SH_MINUS_2 3
#define SH_MINUS_3 2
#define SH_MINUS_4 1
#define SH_MINUS_D 0

// day LEDs - CORRECT
#define SH_MON 1
#define SH_TUE 2
#define SH_WED 3
#define SH_THU 4
#define SH_FRI 5
#define SH_SAT 6
#define SH_SUN 7

// defines for 7 segment indicators
#define SH_DIG_0  1<<SH_A|1<<SH_B|1<<SH_C|1<<SH_D|1<<SH_E|1<<SH_F
#define SH_DIG_1  1<<SH_B|1<<SH_C
#define SH_DIG_2  1<<SH_A|1<<SH_B|1<<SH_D|1<<SH_E|1<<SH_G
#define SH_DIG_3  1<<SH_A|1<<SH_B|1<<SH_C|1<<SH_D|1<<SH_G
#define SH_DIG_4  1<<SH_B|1<<SH_C|1<<SH_F|1<<SH_G
#define SH_DIG_5  1<<SH_A|1<<SH_C|1<<SH_D|1<<SH_F|1<<SH_G
#define SH_DIG_6  1<<SH_A|1<<SH_C|1<<SH_D|1<<SH_E|1<<SH_F|1<<SH_G
#define SH_DIG_7  1<<SH_A|1<<SH_B|1<<SH_C
#define SH_DIG_8  1<<SH_A|1<<SH_B|1<<SH_C|1<<SH_D|1<<SH_E|1<<SH_F|1<<SH_G
#define SH_DIG_9  1<<SH_A|1<<SH_B|1<<SH_C|1<<SH_D|1<<SH_F|1<<SH_G

#define SH_DIG_h  1<<SH_C|1<<SH_E|1<<SH_F|1<<SH_G
#define SH_DIG_grad  1<<SH_A|1<<SH_B|1<<SH_F|1<<SH_G
#define SH_DIG_e  1<<SH_A|1<<SH_D|1<<SH_E|1<<SH_F|1<<SH_G
#define SH_DIG_r  1<<SH_E|1<<SH_G
#define SH_DIG_o  1<<SH_C|1<<SH_D|1<<SH_E|1<<SH_G
#define SH_DIG_L  1<<SH_D|1<<SH_E|1<<SH_F
#define SH_DIG_b  SH_DIG_o | 1<<SH_F
#define SH_DIG_A  SH_DIG_h | 1<<SH_A | 1<<SH_B

// misc
#define Mon 1
#define Tue 2
#define Wed 3
#define Thu 4
#define Fri 5
#define Sat 6
#define Sun 7
#define SH_NOPE 126

// function prototypes
void shift_init();

void shift_out(); // shifts array through SPI port
void shift_arr_reset(); // all elements in array = 0

void shift_arr_set_date(uchar day, uchar mon, uchar year);
void shift_arr_set_day (uchar day);
void shift_arr_set_temp(signed char t1, signed char t2, signed char t3, signed char t4);
void shift_arr_set_cond(uchar c1, uchar c2, uchar c3, uchar c4);
void shift_arr_set_dht (signed char temp, uint8_t hum);
void shift_arr_set_dht_err();
void shift_arr_set_dht_lo_ba();


// INTERNAL USE ONLY
uchar __dig2shift(uchar dig);
uchar __abs(schar);
void __arr_reverse(uchar word[], uchar len);

#endif
