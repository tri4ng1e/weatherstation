/*
    Copyright (c) 2007 Stefan Engelke <mbox@stefanengelke.de>
	Modified by Sky-WaLkeR (aka shadowalker), May 2013 <alex@naboko.tk>

*/

#ifndef _LIB_SPI_H_
#define _LIB_SPI_H_

#include <avr/io.h>

#define PORT_SPI	PORTB
#define DDR_SPI		DDRB
#define DD_MISO		4
#define DD_MOSI		3
#define DD_SCK		5

// Initialize pins for spi communication
void spi_init();

// Shift full array through target device
void spi_transfer_sync (uint8_t * dataout, uint8_t * datain, uint8_t len);

// Shift full array to target device without receiving any byte
void spi_transmit_sync (uint8_t *dataout, uint8_t len);

// Clocks one byte to target device and returns the received one
uint8_t spi_fast_shift (uint8_t data);

#endif /* _LIB_SPI_H_ */
