/*
		v_port.h - header
	   ������: WeatherStation
	���������: MAIN uncomp v4
		�����: Sky-WaLkeR
	
	   ������: 1.0

	 ��������:
  ���������� ������������ ����� ��� ��������.
  �����: ������ ������ ���� ������� � ������ ����� �����-������, ������ ������ ���� - � �������
  � ������ ������ ���� ������������ ����� 3..0 ���� � PORTA3..0, � ���� 7..4 - � PORTB3..0
  ��� ���� ���� ������ �� PORTx, DDRx � PINx.
  �������� ����� �������:
		vport_write( uint8_t data )
uint8_t	vport_read() (������ � PORTx)
uint8_t vpin_read() (������ � PINx)
		vddr_write( uint8_t data )
uit8_t	vddr_read()
  
*/
#ifndef _H_v_port_INC_
#define _H_v_port_INC_

#include <avr/io.h>

#define V_PORT_1 PORTA
#define V_PORT_2 PORTB
#define V_DDR_1  DDRA
#define V_DDR_2  DDRB
#define V_PIN_1  PINA
#define V_PIN_2  PINB


void vport_write(uint8_t value){
	V_PORT_1 = (V_PORT_1 & 0xf0) | (value & 0x0f);
	V_PORT_2 = (V_PORT_2 & 0xf0) | (value & 0xf0) >> 4;
}

void vddr_write(uint8_t value)
{
	V_DDR_1 = (V_DDR_1 & 0xf0) | (value & 0x0f);
	V_DDR_2 = (V_DDR_2 & 0xf0) | (value & 0xf0) >> 4;
}

uint8_t vport_read(){
	return (V_PORT_1 & 0xf0) | (V_PORT_2 & 0xf0) << 4;
}

uint8_t vpin_read(){
	return (V_PIN_1 & 0xf0) | (V_PIN_2 & 0xf0) << 4;
}

//#include "v_port.cpp"
#endif
