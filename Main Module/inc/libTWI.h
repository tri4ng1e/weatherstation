/*
		libTWI.h - header
	   ������: WeatherStation
	���������: MAIN uncomp v4
		�����: Sky-WaLkeR

	   ������: 1.0

	 ��������:
	This tibrary allows you to use hardware TWI (I2C) protocol.

*/
#ifndef _LIB_TWI_H_
#define _LIB_TWI_H_
#define TWI_MODE_HARDWARE

#include <util/twi.h>
#include <avr/io.h>
#include <myLibs/types.h>

//I2C clock in Hz
#define SCL_CLOCK  100000L 

#define I2C_WRITE	0
#define I2C_READ	1

#define I2C_ACK	1
#define I2C_NAK	0

extern void i2c_init(void);

// Sends START cond, address and direction
// return 0 - success, 1 - failed to access device
extern uint8_t i2c_start(uchar address, uchar dir=I2C_WRITE);

// Sends START cond and address (with direction)
// If device is busy, use ack polling to wait until device is ready
extern void i2c_start_wait(uchar address);


/*************************************************************************
 Issues a repeated start condition and sends address and transfer direction

 Input:   address and transfer direction of I2C device

 Return:  0 device accessible
          1 failed to access device
*************************************************************************/
extern uint8_t i2c_rep_start(uint8_t address);


/*************************************************************************
 Terminates the data transfer and releases the I2C bus
*************************************************************************/
extern void i2c_stop(void);


/*************************************************************************
  Send one byte to I2C device

  Input:    byte to be transfered
  Return:   0 write successful
            1 write failed
*************************************************************************/
extern uint8_t i2c_write(uchar data);

/*************************************************************************
 Read one byte from the I2C device and send Ack or Nak

 Input:   mode (I2C_ACK or I2C_NAK ), default I2C_ACK
 Return:  byte read from I2C device
*************************************************************************/
extern uint8_t i2c_read(uchar mode=I2C_ACK);


#endif
