#ifndef _H_dyn_INC_
#define _H_dyn_INC_

#define DYN_DIG_0  1<<DYN_A|1<<DYN_B|1<<DYN_C|1<<DYN_D|1<<DYN_E|1<<DYN_F
#define DYN_DIG_1  1<<DYN_B|1<<DYN_C
#define DYN_DIG_2  1<<DYN_A|1<<DYN_B|1<<DYN_D|1<<DYN_E|1<<DYN_G
#define DYN_DIG_3  1<<DYN_A|1<<DYN_B|1<<DYN_C|1<<DYN_D|1<<DYN_G
#define DYN_DIG_4  1<<DYN_B|1<<DYN_C|1<<DYN_F|1<<DYN_G
#define DYN_DIG_5  1<<DYN_A|1<<DYN_C|1<<DYN_D|1<<DYN_F|1<<DYN_G
#define DYN_DIG_6  1<<DYN_A|1<<DYN_C|1<<DYN_D|1<<DYN_E|1<<DYN_F|1<<DYN_G
#define DYN_DIG_7  1<<DYN_A|1<<DYN_B|1<<DYN_C
#define DYN_DIG_8  1<<DYN_A|1<<DYN_B|1<<DYN_C|1<<DYN_D|1<<DYN_E|1<<DYN_F|1<<DYN_G
#define DYN_DIG_9  1<<DYN_A|1<<DYN_B|1<<DYN_C|1<<DYN_D|1<<DYN_F|1<<DYN_G
#define DYN_DIG_h  1<<DYN_C|1<<DYN_E|1<<DYN_F|1<<DYN_G

uint8_t dyn_dig1=0, dyn_dig2=0, dyn_dig3=0, dyn_dig4=0;

uint8_t __dig2dyn(uint8_t dig){
	switch(dig){
	case 0: return DYN_DIG_0; break;
	case 1: return DYN_DIG_1; break;
	case 2: return DYN_DIG_2; break;
	case 3: return DYN_DIG_3; break;
	case 4: return DYN_DIG_4; break;
	case 5: return DYN_DIG_5; break;
	case 6: return DYN_DIG_6; break;
	case 7: return DYN_DIG_7; break;
	case 8: return DYN_DIG_8; break;
	case 9: return DYN_DIG_9; break;
	case 255: return 0; break;
	}
	return 0;
}

void dyn_init(){
	TCCR1A=1<<WGM10|0<<WGM11; TCCR1B=2|1<<WGM12|0<<WGM13;
	vport_write(0x00);
	vddr_write(0xff);

}

void dyn_set_pwm(uint8_t a){
	asm("cli");
	OCR1BH=0;
	OCR1BL=255-a;
	asm("sei");
}

void dyn_pwm(uint8_t flag=1){
	flag==1? TCCR1A|=1<<COM1B1 : TCCR1A&=~(1<<COM1B1);
}

uint8_t dyn_state=1;
void dyn_event(){
	switch(dyn_state){
	 case 1:{ // �������� � 1-�� digit
		DYN_C4_EN::Clear();
		vport_write(__dig2dyn(dyn_dig1));
		DYN_C1_EN::Set();
		break;
		}
	 case 2:{ // �������� � 2-�� digit
		DYN_C1_EN::Clear();
		vport_write(__dig2dyn(dyn_dig2) | RTC_SQ::IsSet()<<DYN_DOT);
		DYN_C2_EN::Set();
		break;
	 }
	 case 3:{ // �������� � 3-�� digit
		DYN_C2_EN::Clear();
		vport_write(__dig2dyn(dyn_dig3));
		DYN_C3_EN::Set();
		break;
		}
	 case 4:{ // �������� � 4-�� digit
		DYN_C3_EN::Clear();
		vport_write(__dig2dyn(dyn_dig4));
		DYN_C4_EN::Set();
		break;
	 }
//	 case 5:{ // �����
//		DYN_C4_EN::Clear();
//		vport_write(RTC_SQ::IsSet()<<DYN_DOT);
//		DYN_C2_EN::Set();
//		break;
//	 }
	}
	dyn_state==4? dyn_state=1 : dyn_state++;
}

void dyn_void(){
	dyn_dig1=0;
	dyn_dig2=0;
	dyn_dig3=0;
	dyn_dig4=0;
}

#endif
