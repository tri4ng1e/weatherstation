/*
		adc.h - header
	Project: !atm32-ws-DEBUG_adc
	Author:	Admin
	
	Version: 0.1
	   Date: 22.05.2013

	Description:
  description goes here

	Notes:
  goes here  
  
*/
#ifndef _H_adc_INC_
#define _H_adc_INC_
#include <avr/io.h>

void adc_init(){
	ADCSRA=0<<ADATE | 1<<ADPS0; // ���� ���������, ������������ 3
	ADMUX=1<<REFS0 | 1<<ADLAR; // ������������ �� ����� �������, AVCC
}
void adc_start(uint8_t ch){
	ADMUX|=ch;
	ADCSRA|=1<<ADEN | 1<<ADSC; // �������� ADC � ��������� ��������������
}
void adc_wait(){
	while ((ADCSRA & (1<<ADIF)) == 0) {}
}
void adc_start_wait(uint8_t ch){
	adc_start(ch); adc_wait();
}
uint8_t adc_read(){
	return 255 - (ADCH & 0xff);
}

void adc_stop(){
	ADCSRA&=~(1<<ADEN | 1<<ADSC | 1<<ADIF);
}

//#include "adc.cpp"
#endif
